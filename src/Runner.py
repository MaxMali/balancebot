import exchanges.BinanceFutures as binfut
import exchanges.Bybit as bybit
import exchanges.Bitmex as bitmex
import Plotter as plotter
import pickle
import io
import json

exchangeMap = {
    'bitmex': bitmex,
    'binance': binfut,
    'bybit': bybit
}

challenger_data_base_dict = {
    "exchange": None,
    "key": "",
    "secret": "",
    "start_date": "2020-09-01",
    "defaultMarket": "futures",
    'chart_options': {
        'chart_color': 'black',
        'account_name': 'noname'
    }
}

challenger_data = {}

def saveData(data):
    openFile = open('dbData.json', 'w')
    json.dump(data, openFile)
    openFile.close()

def loadData():
    try:
        with open('dbData.json', 'r') as read_file:
            challenger_data = json.load(read_file)
            print("GOT DATA")
            print(challenger_data)
    except IOError:
        saveData(challenger_data)
    return challenger_data

def update_challenger_data(userID, field, value):
    data = loadData()
    if(userID not in data):
        data[userID] = challenger_data_base_dict

    if(field == 'chart_color' or field == 'account_name'):
        data[userID]['chart_options'][field] = value
    else:
        data[userID][field] = value

    saveData(data)

def get_account_for_user(userID):
    data = loadData()
    if(userID not in data):
        return "No User with that ID found"
    else:
        return data[userID]

def generate_image():
    all_data = []
    challenger_configs = loadData()

    for challenger in challenger_configs.values():
        try: 
            all_data.append(dict({'data': exchangeMap[challenger['exchange']].getAccountInformation(challenger), 'chart_options': challenger['chart_options']}))
        except Error:
            print(f"Could not Complete Chart for: {challenger}")

    filename = plotter.plot_all_challengers(all_data)
    
    return filename

def generate_image_single_user(userID):
    challenger = get_account_for_user(userID)
    filename = plotter.single_plot_picture(exchangeMap[challenger['exchange']].getAccountInformation(challenger), challenger['chart_options'])

    return filename