import os
import plotly.graph_objects as go
import datetime
import pandas as pd


def create_plot(data_dict, chart_options, fig):
    date_array = data_dict['pnl_df']['timestamp'].tolist()
    values_array = data_dict['pnl_df']['delta_to_start'].tolist()
    fig.add_trace(go.Scatter(x=date_array, y=values_array,
        mode='lines+markers+text',
        text=[round(x, 2) for x in values_array],
        name=f"{chart_options['account_name']} - {round(values_array[-1], 2)}%",
        textposition = 'bottom center',
        marker= {
            'color': chart_options['chart_color'],
        },
        line= {
            'color': chart_options['chart_color']
        },
        textfont = {
            'color': chart_options['chart_color']
        }))
    # fig.update_xaxes(range=[date_array[0], date_array[-1] + pd.Timedelta(hours=12)])
    fig.update_layout(showlegend=True)

    return fig

def save_image(fig,file_name):
    if not os.path.exists("images"):
        os.mkdir("images")
    fname = f"images/{file_name}.jpeg"
    fig.write_image(fname)
    return fname

def single_plot_picture(data_dict, chart_options):
    fig = go.Figure()
    fig_plot = create_plot(data_dict, chart_options, fig)
    print(chart_options)
    return save_image(fig_plot, chart_options['account_name'])

def plot_all_challengers(challenger_data):
    fig = go.Figure()
    for challenger in challenger_data:
        fig = create_plot(challenger['data'], challenger['chart_options'], fig)
    
    return save_image(fig, 'ALL')